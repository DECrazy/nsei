from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length = 25)

    def __str__(self):
        return self.name

class Post(models.Model):
    class Meta:
        ordering = ['-create_at']
    author = models.CharField(max_length=45)
    create_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    img = models.ImageField(null=True, blank=True)
    description = RichTextField()
    text = RichTextField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True, related_name = 'posts')

    def __str__(self):
        return self.title

class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.post

class Dislike(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.post

class Comment(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name = 'comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name = 'comments')

    def __str__(self):
        return self.text

class Termos_de_Uso(models.Model):
    text = models.TextField()
    par1 = models.TextField(blank=True, null=True)
    par2 = models.TextField(blank=True, null=True)
    par3 = models.TextField(blank=True, null=True)
    par4 = models.TextField(blank=True, null=True)
    par5 = models.TextField(blank=True, null=True)
    par6 = models.TextField(blank=True, null=True)
    par7 = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.text

        


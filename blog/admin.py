from django.contrib import admin

from blog.models import Post, Category, Like, Dislike, Comment, Termos_de_Uso



admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Like)
admin.site.register(Dislike)
admin.site.register(Comment)
admin.site.register(Termos_de_Uso)